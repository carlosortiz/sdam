import os
from pathlib import Path

import boto3


class S3Client:
    def __init__(self):
        self.s3 = boto3.resource("s3")
        self.bucket = self.s3.Bucket(os.environ["SDAM_AWS_BUCKET"])

        self.lws3 = boto3.client('s3')

    def list(self, path):
        result = self.lws3.list_objects_v2(Bucket=self.bucket.name, Prefix=path, Delimiter="/")
        if 'Contents' in result:
            files = result["Contents"]
        else:
            files = []
        if 'CommonPrefixes' in result:
            folders = result["CommonPrefixes"]
        else:
            folders = []
        return self.__folders_to_map(folders) + self.__files_to_map(files)

    def sign_url(self, file):
        return self.lws3.generate_presigned_url('get_object',
                                                Params={'Bucket': self.bucket.name,
                                                        'Key': file},
                                                ExpiresIn=3600)

    @staticmethod
    def __files_to_map(s3_files):
        files = []
        for f in s3_files:
            print(f)
            key = f["Key"]
            if key.endswith("/"):
                continue
            files.append({
                "type": "FILE",
                "name": Path(key).name,
                "path": key,
                "size": f["Size"],
                "lastModified": f["LastModified"]
            })
        return files

    @staticmethod
    def __folders_to_map(s3_folders):
        folders = []
        for f in s3_folders:
            folders.append({
                "type": "FOLDER",
                "name": f["Prefix"],
                "path": f["Prefix"]
            })
        return folders
