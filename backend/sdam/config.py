import yaml


class Configuration():
    def __init__(self):
        with open(r'configuration.yaml') as file:
            self.config = yaml.load(file, Loader=yaml.FullLoader)

    def get(self, key):
        return self.config.get(key)


config = Configuration()
