import connexion

from sdam.aws import s3Client
from sdam.models.request_path import RequestPath  # noqa: E501
from sdam.models.list_dir_response import ListDirResponse  # noqa: E501


def list_dir(body=None):  # noqa: E501
    """list_dir

    List directory contents # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: ListDirResponse
    """
    if connexion.request.is_json:
        body = RequestPath.from_dict(connexion.request.get_json())  # noqa: E501
    return ListDirResponse.from_dict(s3Client.list(body.path))
