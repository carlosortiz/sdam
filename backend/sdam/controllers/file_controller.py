import connexion

from sdam.aws import s3Client
from sdam.models.get_url_response import GetUrlResponse  # noqa: E501
from sdam.models.request_path import RequestPath  # noqa: E501


def get_signed_url(body=None):  # noqa: E501
    """get_signed_url

    Get Signed Url for the given file # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: GetUrlResponse
    """
    if connexion.request.is_json:
        body = RequestPath.from_dict(connexion.request.get_json())  # noqa: E501
        return GetUrlResponse.from_dict({"url": s3Client.sign_url(body.path)})
    return {}
