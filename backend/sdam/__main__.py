#!/usr/bin/env python3

import connexion
from flask_cors import CORS

from sdam import encoder


def main():
    app = connexion.App(__name__, specification_dir='./swagger/', debug=True)
    CORS(app.app, resources={r"/v1/*": {"origins": "*"}})
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Simple Digital Asset Manager'}, pythonic_params=True,
                base_path='/v1')
    app.run(port=8080)
    return app


if __name__ == '__main__':
    main()
